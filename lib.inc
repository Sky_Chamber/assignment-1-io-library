section .text
%define WRITE 1
%define READ 0
%define EXIT 60


 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov     rax, EXIT
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
	je .END
	inc rax
	jmp .loop
    .END:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, WRITE
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi 
    mov rax, WRITE
    mov rdi, 1 
    mov rsi, rsp 
    mov rdx, 1 
    syscall
    pop rdi 
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, 10
    mov rsi, rsp
    sub rsp, 21
    dec rsi
    mov byte [rsi], 0
    dec rsi 
    .loop:
        xor rdx,rdx
        div rdi
	add rdx, 0x30
	mov [rsi], dl 
	dec rsi
	cmp rax, 0
	je .PRINT
	jmp .loop
    .PRINT:
        inc rsi
        mov rdi, rsi
        call print_string
    add rsp, 21
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .NEG
    call print_uint
    jmp .END
    .NEG:
        neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	call print_uint
    .END:
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    call string_length
    pop rdi
    push rax; save length first string
    push rdi
    push rsi
    mov rdi, rsi
    call string_length
    pop rsi
    pop rdi; restore two pointers after computing second length
    pop r11; here is first length, second is in rax
    cmp rax, r11
    jne .FALSE; if we go further we know length1 == length2
    cmp rax, 0
    je .TRUE
    .loop:
        mov r11b, [rdi]
        cmp r11b, [rsi]
	jne .FALSE
	inc rdi
	inc rsi
        dec rax
	jne .loop
    .TRUE:
        mov rax, 1
	jmp .END
    .FALSE:
        xor rax, rax
    .END:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    dec rsp
    mov rsi, rsp
    syscall
    test rax, rax
    je .EOF
    mov al, [rsp]
    .EOF:
        inc rsp
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    push rdi
    push rsi
    .A:
        call read_char
	cmp rax, 0x20
	je .A
	cmp rax, 0x9
	je .A
	cmp rax, 0xA
	je .A
    .B:
        pop rsi
	pop rdi
        test rsi, rsi
	je .WRONG
	mov [rdi], al
	test rax, rax
	je .RIGHT
	cmp rax, 0x20
	je .RIGHT
	cmp rax, 0x9
	je .RIGHT
	cmp rax, 0xA
	je .RIGHT
	inc rdi
	dec rsi
	push rdi
	push rsi
	call read_char
	jmp .B
    .RIGHT:
        mov byte [rdi], 0
        mov rdi, [rsp]
        call string_length
	mov rdx, rax
	pop rax
	jmp .END
    .WRONG:
        pop rax
        xor rax, rax
    .END:
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor rdx, rdx
    mov r11, 10
    .A:
        push rsi
        mov sil, [rdi]
	test sil, sil
	je .END
	cmp sil, 10
	je .END
	cmp sil, '0'
	jb .END
	cmp sil, '9'
	ja .END
        sub sil, 0x30
	mul r11
	add rax, rsi
	inc rdi
	pop rsi
	inc rsi
	jmp .A
    .END:
        pop rdx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi; save pointer
    call parse_uint
    pop rdi
    test rdx, rdx
    jne .END; already parsed
    mov sil, [rdi]; here is the sign, it will come later
    inc rdi
    push rsi
    call parse_uint
    inc rdx
    pop rsi
    cmp sil, '-'
    jne .END
    neg rax 
    .END:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdx
    push rsi
    push rdi
    call string_length
    inc rax
    pop rdi
    pop rsi
    pop rdx
    cmp rax, rdx
    jg .WRONG_LENGTH
    push rax
    .loop:
        mov r11b, [rdi]
	mov [rsi], r11b
	inc rdi
	inc rsi
        dec rax
	jne .loop
    pop rax
    jmp .END
    .WRONG_LENGTH:
        xor rax, rax
    .END:
        ret
